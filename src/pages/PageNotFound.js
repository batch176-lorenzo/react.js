import React from 'react'
import { Link } from 'react-router-dom';



export default function PageNotFound () {
	return (
		<>
			<h3>Zuitt Booking</h3>
			<h1>Page Not Found</h1>
			<p>
				Go back to the <Link to="/">homepage</Link>
			</p>
		</>
	)	
}