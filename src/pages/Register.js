import { useState, useEffect, useContext } from 'react'
import { Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom'

export default function Register (){
	const navigate = useNavigate();
	const { user } = useContext(UserContext);
	//state hooks to store the values of the input fields
	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ]	= useState('')
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');
	const [ mobileNo, setMobileNo] = useState('');

	//state for the enable/disable button
	const [ isActive, setIsActive ] = useState(true);

	useEffect(() =>{
		//validation to enable submit button
		if ((firstName !== '' && lastName !== '' && email !== '' && password !== '' && verifyPassword !== '' && mobileNo !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else {
			setIsActive(false);	
		}
	}, [firstName, lastName, email, password, verifyPassword, mobileNo])

	function registerUser(e) {
		e.preventDefault();
		
		fetch('https://educatin-management-system.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password,
				verifyPassword: verifyPassword,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
						title: 'Yeheeeeyyyy',
						icon: 'success',
						text: 'You have successfully registered!'
					})
			navigate('/login')

		})
		setFirstName('')
		setLastName('')
		setEmail('')
		setPassword('')
		setVerifyPassword('')
		setMobileNo('')
	}


	return(
		(user.accessToken !== null) ?

		<Navigate to="/" />

		:

		<Form className="mt-3" onSubmit={e => registerUser(e)}>
			<h1>Register </h1>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your First Name"
					required
					value={ firstName }
					onChange={ e => setFirstName(e.target.value)}
					/>	
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter your Last Name"
					required
					value={ lastName }
					onChange={ e => setLastName(e.target.value)}
					/>	
			</Form.Group>

			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={ email }
					onChange={ e => setEmail(e.target.value)}
					/>	
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your Password"
					required
					value={ password }
					onChange={ e => setPassword(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your password with anyone else.
					</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					required
					value= { verifyPassword }
					onChange={ e => setVerifyPassword(e.target.value)}
					/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter your Mobile Number"
					required
					value={ mobileNo }
					onChange={ e => setMobileNo(e.target.value)}
					/>	
			</Form.Group>
			{isActive ? 
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>	
			}
		</Form>
		)

}