import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function ArchiveCourse({ course, isActive, fetchData }){

	const archiveToggle = (courseId) => {
		fetch(`https://educatin-management-system.herokuapp.com/courses/${courseId}/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully disabled'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'erro',
					text: 'Something Went Wrong'
				})
			}
		})
	}

	const activateToggle = (courseId) => {
		fetch(`https://educatin-management-system.herokuapp.com/courses/${courseId}/activate`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully Enabled'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Error',
					icon: 'erro',
					text: 'Something Went Wrong'
				})
			}
		})
	}

	return(

		<>
			{ isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(course)}>
				Disable
				</Button>
				:
				<Button variant="success" size="sm" onClick={() => activateToggle(course)}>
				Enable
				</Button>
			}

			
		</>
		)
}